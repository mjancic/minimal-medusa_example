# Minimal Medusa example

Minimal medusa example.

## Including medusa to project

Include medusa via the `CMakeLists.txt` file, where directory to the medusa should be fixed (if required).

```cmake
project(example)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_CXX_FLAGS "-std=c++17 -fopenmp -O3 -DNDEBUG")

add_subdirectory(${CMAKE_SOURCE_DIR}/../medusa/ medusa)     # <----- Modify path if required.
include_directories(${CMAKE_SOURCE_DIR}/../medusa/include/) # <----- Modify path if required.

add_executable(example source/main.cpp)
target_link_libraries(example medusa)  # link to our library
```

## Compile

To compile use VSC or do it manually by navigating to `root/build` directory and running

```bash
cmake .. && make -j 12
```

Executable will be created in `root/bin/` directory.
